package io.codementor.gtommee.rest_tutorial.controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.codementor.gtommee.rest_tutorial.repositories.PetsRepository;
import org.bson.Document;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/respuestas")
public class PetsController {
    @Autowired
    private PetsRepository repository;

    @Autowired
    private MongoTemplate mongoTemplate;

//    @RequestMapping(value = "/", method = RequestMethod.GET)
//    public List<Pets> getAllPets() {
//        return repository.findAll();
//    }
//
////    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
////    public Pets getPetById(@PathVariable("id") ObjectId id) {
////        return repository.findBy_id(id);
////    }
//
//    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
//    public void modifyPetById(@PathVariable("id") ObjectId id, @Valid @RequestBody Pets pets) {
//        pets.set_id(id);
//        repository.save(pets);
//    }
//
//    @RequestMapping(value = "/", method = RequestMethod.POST)
//    public Pets createPet(@Valid @RequestBody Pets pets) {
//        pets.set_id(ObjectId.get());
//        repository.save(pets);
//        return pets;
//    }
//
//    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
//    public void deletePet(@PathVariable ObjectId id) {
//        repository.delete(repository.findBy_id(id));
//    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    ResponseEntity<?> add(@RequestBody String jsonString) {

        Map<String, String> keys = new HashMap<>();

        try {
            ObjectMapper objectMapper = new ObjectMapper();
            JsonNode rootNode = objectMapper.readTree(jsonString);

            if (rootNode.isArray()) {

                for (JsonNode item : rootNode) {
                    String key = item.get("id").toString();
                    String json = new ObjectMapper().writer().writeValueAsString(item);
                    Document doc = Document.parse(json);
                    ObjectId respuestaId = mongoTemplate.insert(doc, "respuestas").getObjectId("_id");
                    keys.put(key, respuestaId.toHexString());
                    System.out.println(respuestaId.toHexString());
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return new ResponseEntity<>(keys, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> getRespuestaById(@PathVariable("id") ObjectId id) {
        repository.findBy_id(id);

        Map envinfo= mongoTemplate.findById(id, Map.class, "respuestas");

        return new ResponseEntity<>(envinfo, HttpStatus.OK);
    }

}
